# Repository Name

DarkJarris-Toolbag

# Installation

These are cogs for the Red-DiscordBot V3. You need to have a working V3 Redbot in order to install these cogs.

[p] is your prefix.

Make sure downloader is loaded:
[p]load downloader

Add the repo to your bot:
[p]repo add DarkJarris-Toolbag https://bitbucket.org/DarkJarris/darkjarris-toolbag

Install the cogs you want:
[p]cog install DarkJarris-Toolbag <cog name>

Load installed cogs:
[p]load <cog name>

# Contact

Information on how to contact you about issues, bugs, and enhancements to your work

# Credits

Cite credit to anyone you collaborated with, or authors of code you used in your work that is not your own.
