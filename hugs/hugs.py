from redbot.core import commands
import discord
import random


# Classname should be CamelCase and the same spelling as the folder
class DarkJarrishugs(commands.Cog):
    """Description of the cog visible with [p]help MyFirstCog"""

    @commands.command()
    async def hugs(self, ctx):
        gif = random.randrange(1,9)
        """posts a hug gif. takes no arguments"""
        # Your code will go here
        if gif == 1:
            await ctx.send("https://media1.tenor.com/images/dd1b8fe694d7bfba2ae87e1ede030244/tenor.gif")
        if gif == 2:
            await ctx.send("https://media1.tenor.com/images/110e60458d7daaf59fd9845abd53ec88/tenor.gif")
        if gif == 3:
            await ctx.send("https://media1.tenor.com/images/b67b70e46deb55aef87a7c744e460373/tenor.gif")
        if gif == 4:
            await ctx.send("https://media.discordapp.net/attachments/290621596509667329/820460653402914846/tenor.gif")
        if gif == 5:
            await ctx.send("https://media1.tenor.com/images/24ac13447f9409d41c1aecb923aedf81/tenor.gif")
        if gif == 6:
            await ctx.send("https://media1.tenor.com/images/11b756289eec236b3cd8522986bc23dd/tenor.gif")
        if gif == 7:
            await ctx.send("https://media1.tenor.com/images/d7529f6003b20f3b21f1c992dffb8617/tenor.gif")
        if gif == 8:
            await ctx.send("https://media1.tenor.com/images/f2fe21d5b4fc2f4a258e4708b10c966c/tenor.gif")
        if gif == 9:
            await ctx.send("https://media1.tenor.com/images/3fee00811a33590e4ee490942f233c78/tenor.gif")
        